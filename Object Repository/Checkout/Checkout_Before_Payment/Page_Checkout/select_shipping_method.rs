<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_shipping_method</name>
   <tag></tag>
   <elementGuidId>5365fe38-13da-4fc5-96d3-bc315e2ef5a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='shipping_method']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;shipping_method&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sel_btc_shipping</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shipping_method</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-seller-id</name>
      <type>Main</type>
      <value>7093</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                                                Express (1 Hari)
                                                                                                                                            
                                                                              Tiki - ONS (Rp 18,000)
                                                                        
                                                                                                                                Instant (1 Hari)
                                                                                                                                            
                                                                              Grab Express - Instant (Rp 14,000)
                                                                        
                                                                                                                                Regular (2 Hari)
                                                                                                                                            
                                                                              Tiki - REG (Rp 9,000)
                                                                        
                                                                                                                                Same Day (1 Hari)
                                                                                                                                            
                                                                              Grab Express - Same Day (Bulk) (Rp 15,000)
                                                                        
                                                                                                                                    
                                                                              Grab Express - Same Day (Direct) (Rp 15,000)
                                                                        
                                                                                                                                    
                                                                              GO-SEND - Same Day (Bulk) (Rp 18,000)
                                                                        
                                                                                                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form_checkoutCart&quot;)/div[@class=&quot;row row_masterCart&quot;]/div[@class=&quot;col-md-12 col_masterCart&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7 col_checkout_page1&quot;]/div[@class=&quot;bx_checkout_page&quot;]/div[@class=&quot;row row_btc_shipping&quot;]/div[@class=&quot;f_sel_shipping&quot;]/select[@class=&quot;sel_btc_shipping&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='shipping_method']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_checkoutCart']/div/div/div[3]/div/div[2]/div[4]/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Metode Pengiriman'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='%'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Biaya Pengiriman'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 18,000'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
