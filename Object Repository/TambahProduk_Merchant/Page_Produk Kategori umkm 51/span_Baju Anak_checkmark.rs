<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Baju Anak_checkmark</name>
   <tag></tag>
   <elementGuidId>88c25617-b47c-468e-b99d-39e360d72b40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v_subfilter_categories']/div[28]/label/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label.cont_check.cont_check_category_child.checkbox_category_child_51 > span.checkmark</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkmark</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;v_subfilter_categories&quot;)/div[@class=&quot;f_aform f_aform_brand f_aform_child checkbox_category_child_container_51&quot;]/label[@class=&quot;cont_check cont_check_category_child checkbox_category_child_51&quot;]/span[@class=&quot;checkmark&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v_subfilter_categories']/div[28]/label/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[28]/label/span</value>
   </webElementXpaths>
</WebElementEntity>
