<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Hi, Wisnu                              _acc6c1</name>
   <tag></tag>
   <elementGuidId>30a93c5f-80e9-47d7-b2a0-94951b8998f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

        
        

          
          
            
          

          
          

            
            
              
                
              
              
              
                
                  
                    
                    
                      
                        
                      
                    
                  
                
              
            




            

            
            
            
              
                Hi, Wisnu
                
              
              
              
                
                  
                  Logout
                
              
            

          

        
        
	


            
            
                
                Data Toko
                
                
                
                    
                        Detail Toko
                    
                    
                        
                            
                                
                                    Nama Toko
                                    
                                
                            
                            
                                
                                    Provinsi
                                    
                                                                                    Aceh
                                                                                    Bali
                                                                                    Bangka Belitung
                                                                                    Banten
                                                                                    Bengkulu
                                                                                    DI Yogyakarta
                                                                                    DKI Jakarta
                                                                                    Gorontalo
                                                                                    Jambi
                                                                                    Jawa Barat
                                                                                    Jawa Tengah
                                                                                    Jawa Timur
                                                                                    Kalimantan Barat
                                                                                    Kalimantan Selatan
                                                                                    Kalimantan Tengah
                                                                                    Kalimantan Timur
                                                                                    Kalimantan Utara
                                                                                    Kepulauan Riau
                                                                                    Lampung
                                                                                    Maluku
                                                                                    Maluku Utara
                                                                                    new province
                                                                                    Nusa Tenggara Barat
                                                                                    Nusa Tenggara Timur
                                                                                    Papua
                                                                                    Papua Barat
                                                                                    Riau
                                                                                    Sulawesi Barat
                                                                                    Sulawesi Selatan
                                                                                    Sulawesi Tengah
                                                                                    Sulawesi Tenggara
                                                                                    Sulawesi Utara
                                                                                    Sumatera Barat
                                                                                    Sumatera Selatan
                                                                                    Sumatera Utara
                                                                            
                                
                            
                            
                                
                                    URL Toko
                                    
                                
                            
                            
                                
                                    Kota
                                    
                                                                                    Bandung Barat
                                                                                    Bandung, Kab.
                                                                                    Bandung, Kota
                                                                                    Banjar
                                                                                    Bekasi, Kab.
                                                                                    Bekasi, Kota
                                                                                    Bogor, Kab.
                                                                                    Bogor, Kota
                                                                                    Ciamis
                                                                                    Cianjur
                                                                                    Cimahi
                                                                                    Cirebon, Kab.
                                                                                    Cirebon, Kota
                                                                                    Depok
                                                                                    Garut
                                                                                    Indramayu
                                                                                    Karawang
                                                                                    Kuningan
                                                                                    Majalengka
                                                                                    Pangandaran
                                                                                    Purwakarta
                                                                                    Subang
                                                                                    Sukabumi, Kab.
                                                                                    Sukabumi, Kota
                                                                                    Sumedang
                                                                                    Tasikmalaya, Kab.
                                                                                    Tasikmalaya, Kota
                                                                            
                                
                            
                            
                                
                                    Nama Penjual
                                    
                                
                            
                            
                                
                                    Kecamatan
                                    
                                                                                    Jati Sampurna
                                                                                    Pondok Melati
                                                                                    Jatiasih
                                                                                    Pondok Gede
                                                                                    Mustika Jaya
                                                                                    Bantar Gebang
                                                                                    Bekasi Selatan
                                                                                    Bekasi Barat
                                                                                    Medan Satria
                                                                                    Bekasi Utara
                                                                                    Rawalumbu
                                                                                    Bekasi Timur
                                                                            
                                
                            
                            
                                
                                    No. Handphone Penjual
                                    
                                
                            
                            
                                
                                    Kelurahan
                                    
                                                                                    Jatibening Baru
                                                                                    Jatimakmur
                                                                                    Jaticempaka
                                                                                    Jatiwaringin
                                                                                    Jatibening
                                                                            
                                
                            
                            
                                
                                    Status Keaktifan Toko
                                    
                                        Tidak Aktif
                                        Aktif
                                        Suspend
                                    
                                
                            
                            
                                
                                    UMKM
                                    
                                        Tidak Aktif
                                        Terdaftar Sebagai Anggota Pasar UMKM
                                    
                                
                            
                            
                                
                                    LaDaRa Store
                                    
                                        Tidak Aktif
                                        Terdaftar Sebagai Anggota LaDaRa Store
                                    
                                
                            
                            
                                
                                    Dekranas
                                    
                                                                                    Pilih Dekranas
                                                                                    Pulau Jawa
                                                                                    Jawa Tengah
                                                                                    Jakarta
                                                                                    Pulau Sumatera
                                                                                    Sumatra Barat
                                                                                    Lampung
                                                                                    Jambi
                                                                                    Bali
                                                                                    Sulawesi Selatan
                                                                            
                                
                            
                            
                                
                                    Kode Pos
                                    
                                
                            
                            
                                
                                    Persentase Komisi Ladara (%)
                                    
                                
                            
                            
                                
                                    Latitude
                                    
                                
                            
                            
                                
                                    Longitude
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        Kembali
                                    
                                
                            
                            
                                
                                    Submit
                                
                            
                        
                    
                
            
            
        
          </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='content']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content-wrapper']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori Top up'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Top up &amp; Tagihan'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
