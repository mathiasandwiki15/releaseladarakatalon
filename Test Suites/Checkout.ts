<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4cd6b2f7-4279-480c-ba26-8178019c975b</testSuiteGuid>
   <testCaseLink>
      <guid>468a0d37-9636-43fd-8a4f-2911b6ae0ba1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/Checkout Before Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdd15f2f-e7c4-4339-85e1-5e976e8d9651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/Checkout Isi Alamat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4aa1c135-efb7-4496-9d52-efefa6b0bd19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/Checkout From Detail Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>471993bd-fa49-4a00-b674-57d0a028d325</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/Checkout From Category Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12b9cfd4-5513-4376-b36d-12cb187315f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/Checkout From Product Brand</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
