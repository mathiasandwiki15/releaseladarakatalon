<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>310ef07f-a93e-468b-80e4-55dd115fe8f1</testSuiteGuid>
   <testCaseLink>
      <guid>a2d9c9c8-4095-4ff6-93ee-81d91967e0f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b262c39-ca6a-4797-9f29-ac92acf1a580</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>773aae49-254a-4eb2-9166-184194c5f80d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/CreditCard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23d45665-5c13-4256-9525-2a7264154eb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06bcf863-7a72-4fc8-a65f-5f63a8f70b43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8214f903-6f69-49fc-a878-c4d34e26a6cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/Mandiri Virtual Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df513986-b075-4996-95ec-85366f7d6d43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Method/Permata</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
