<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Pengaturan_Merchant</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9d256f43-d226-4b06-bc39-2dd369dc8f34</testSuiteGuid>
   <testCaseLink>
      <guid>750959ae-a18d-4140-8a25-8a6ed37cce82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Merchant/Pengaturan_Toko/CatatanToko_Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e21c0e94-562f-4e2d-bc83-f202e78e5078</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Merchant/Pengaturan_Toko/Informasi_Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46f7c218-b68d-447d-9aa5-30484a591067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Merchant/Pengaturan_Toko/Location_Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb69cc7e-07ee-457f-a9df-991626391b55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Merchant/Pengaturan_Toko/Pengiriman_Merchant</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
