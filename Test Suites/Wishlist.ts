<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Wishlist</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>647c4da3-4c32-4f05-9b8e-8ccb388ca8f2</testSuiteGuid>
   <testCaseLink>
      <guid>ae2bd188-79eb-4c76-ab70-1f13044f064a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Add Product To Wishlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c4768b4-c032-463d-b27b-25791cd063f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Get Product in Wishlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad6b4210-f46e-4a07-907a-47724cf090d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Delete Product in Wishlist</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
