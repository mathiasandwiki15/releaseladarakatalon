<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Shipping</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b2388826-a2fb-4983-8b5f-6c52df4fc1bf</testSuiteGuid>
   <testCaseLink>
      <guid>2996fc86-6ed3-4d04-8b7a-06ecec468e51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Shipping/Shipping - JNE OKE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>375cabb3-acc9-4408-9985-d21a683c5348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Shipping/Shipping - JT REG</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5276da6-78dc-4151-93b7-609422df94a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Shipping/Shipping-JNE YES</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db0b9991-b3eb-4ee4-bb97-ede813272b2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Shipping/Shipping-JNE JTR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5375e4d-8d1a-4459-91e7-8f4c178bc306</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Shipping/Shipping-JTR250</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
