import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_/span_Mathias'))

WebUI.delay(3)

WebUI.click(findTestObject('AddAlamat/Page_/a_Pengaturan'))

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_Address/Page_Address/alamat'))

WebUI.click(findTestObject('AddAlamat/Page_Address/button_Tambah Alamat'))

WebUI.setText(findTestObject('AddAlamat/Page_Add Address/input__nama_alamat'), 'Rumah Pribadi')

WebUI.setText(findTestObject('AddAlamat/Page_Add Address/input__nama_lengkap'), 'Mathias Brait Andwiki Sitohang')

WebUI.setText(findTestObject('AddAlamat/Page_Add Address/input__nomor_hp'), '081286281318')

WebUI.setText(findTestObject('AddAlamat/Page_Add Address/textarea__alamat'), 'Jalan Testing')

WebUI.selectOptionByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Provinsi'), 'Jawa Barat', false)

WebUI.verifyOptionPresentByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Provinsi'), 'Jawa Barat', false, 
    60)

WebUI.selectOptionByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kota'), 'Bekasi, Kota', false)

WebUI.verifyOptionPresentByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kota'), 'Bekasi, Kota', false, 
    60)

WebUI.selectOptionByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kecamatan'), 'Pondok Gede', false)

WebUI.verifyOptionSelectedByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kecamatan'), 'Pondok Gede', false, 
    60)

WebUI.selectOptionByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kode Pos'), '17412 (Jatibening Baru)', 
    false)

WebUI.verifyOptionPresentByLabel(findTestObject('AddAlamat/Page_Add Address/select_Pilih Kode Pos'), '17412 (Jatibening Baru)', 
    false, 0)

WebUI.setText(findTestObject('AddAlamat/Page_Add Address/input_Pin Alamat_address'), 'Depkes 3')

WebUI.click(findTestObject('AddAlamat/Page_Add Address/span_Alamat Utama_checkmark'))

WebUI.click(findTestObject('AddAlamat/Page_Add Address/span_Alamat Utama_checkmark'))

WebUI.click(findTestObject('AddAlamat/Page_Add Address/input_Sukses,_wp-submit'))

