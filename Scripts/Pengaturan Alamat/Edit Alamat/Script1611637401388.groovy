import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_/span_Mathias'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/UpdateProfileRepository/Page_/a_Pengaturan'))

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_Address/Page_Address/alamat'))

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_Address/Page_Address/span_Ubah'))

WebUI.setText(findTestObject('Object Repository/AddAlamat/Page_Add Address/textarea__alamat'),"Jalan Kesehatan VII No B29")

WebUI.setText(findTestObject('Object Repository/AddAlamat/Page_Add Address/input__nomor_hp'), "08123278581")

WebUI.click(findTestObject('Object Repository/AddAlamat/Page_Add Address/input_Sukses,_wp-submit'))



