import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser("https://staging.ladara.id/login#!")
WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), "mathias.brait@idstar.co.id")
WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), "Testing123")
WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 20000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 20000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))
WebUI.navigateToUrl("https://staging.ladara.id/")
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 50000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 50000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))
WebUI.navigateToUrl("https://staging.ladara.id/")
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 100000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 100000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))
WebUI.navigateToUrl("https://staging.ladara.id/")
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 200000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 200000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))
WebUI.navigateToUrl("https://staging.ladara.id/")
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 500000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 500000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))
WebUI.navigateToUrl("https://staging.ladara.id/")
WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), "081282681318")
WebUI.selectOptionByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 1000000", false)
WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), "Rp 1000000", false, 60)
WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_/Page_Ladara/div_Bank Rakyat Indonesia'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/button_Bayar Sekarang'))
WebUI.click(findTestObject('Object Repository/PPOB/Page_Ladara/a_DEV Simulasi pembayaran'))