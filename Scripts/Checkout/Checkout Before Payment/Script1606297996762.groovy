import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5) 

WebUI.setText(findTestObject('Object Repository/Wishlist dan Searching Box/Page_/input_tutup_keyword'), 'Produk Dekranas Pulau Jawa')

WebUI.click(findTestObject('Object Repository/Wishlist dan Searching Box/Page_/span_tutup_glyphicon glyphicon-search sp_searchNow submit-search'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/product_Dekranas_PJawa'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/pilih_varian_merah'))

WebUI.click(findTestObject('Object Repository/CheckoutBeforePayment/Page_Jual Bunga Penangkal Sial/input_Sukses menambahkan produk ini ke keranjang belanja Anda._sub_buyProduct sub_addtoCart'))

WebUI.click(findTestObject('Object Repository/CheckoutBeforePayment/Page_Cart/input_Rp 150,000_sub_cartCheckout'))

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/a_Pilih Alamat Lain'))

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/a_Pilih'))

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/input_Rp 225,000_sub_cartCheckout'))

WebUI.click(findTestObject('Object Repository/Payment/Page_Payment/div_Pilih Bank'))

WebUI.click(findTestObject('Object Repository/Payment/Page_Payment/div_BNI VIRTUAL ACCOUNT'))

WebUI.check(findTestObject('Object Repository/Checkout/Page_Payment/span_Syarat  Ketentuan_checkmark'))

WebUI.click(findTestObject('Object Repository/Checkout/Page_Payment/input_Syarat  Ketentuan_sub_cartCheckout mob_sub_paymentNow'))

WebUI.closeBrowser()