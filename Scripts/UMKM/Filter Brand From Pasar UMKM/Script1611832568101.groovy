import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'testingkatalon13@gmail.com')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('UMKM/Page_1611820991483/Element_1611820998066'), 5)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_1611820991483/Element_1611820998066'), 5)

WebUI.navigateToUrl('https://staging.ladara.id/product-market/umkm')

WebUI.waitForPageLoad(5)

url_umkm = WebUI.getUrl()

WebUI.verifyMatch(url_umkm, 'https://staging.ladara.id/product-market/umkm', true)

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Panasonic_checkmark'))

WebUI.delay(10)

WebUI.scrollToElement(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Panasonic'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Panasonic'), 
    0)

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Panasonic_checkmark'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Namuslimahkecil_checkmark'))

WebUI.delay(10)

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_NamuslimahKecil'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_NamuslimahKecil'), 0)

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Namuslimahkecil_checkmark'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Homzace_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Homzace'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Homzace'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/span_Homzace_checkmark'))

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_paris_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Paris'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Paris'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_paris_checkmark'))

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_scrub body shop_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_ScrubBodyShop'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_ScrubBodyShop'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_scrub body shop_checkmark'))

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Starbucks_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Starbucks'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Starbucks'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Starbucks_checkmark'))

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Test brand_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Test_Brand'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Test_Brand'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Test brand_checkmark'))

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Uninam_checkmark'))

WebUI.scrollToElement(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Uninam'), 0)

WebUI.verifyElementPresent(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/Product_Brand_Uninam'), 0)

WebUI.delay(10)

WebUI.click(findTestObject('UMKM/Page_PASAR UMKM - LaDaRa/span_Uninam_checkmark'))

WebUI.closeBrowser()

