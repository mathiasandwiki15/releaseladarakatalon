import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/shop')

WebUI.check(findTestObject('Object Repository/Produk Sorting Filter/Page_Shop/span_Aceh_checkmark'))

WebUI.verifyTextPresent('Aceh', false)

WebUI.delay(10)

WebUI.check(findTestObject('Object Repository/Produk Sorting Filter/Page_Shop/span_Aceh_checkmark'))

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Riau_checkmark'))

WebUI.verifyTextPresent('Riau', false)

WebUI.delay(10)

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Riau_checkmark'))

WebUI.check(findTestObject('Object Repository/Produk Sorting Filter/Page_Shop/span_Dki jakarta_checkmark'))

WebUI.verifyTextPresent('Dki jakarta', false)

WebUI.delay(10)

WebUI.check(findTestObject('Object Repository/Produk Sorting Filter/Page_Shop/span_Dki jakarta_checkmark'))

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Jawa barat_checkmark'))

WebUI.delay(10)

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Jawa barat_checkmark'))

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Jawa timur_checkmark'))

WebUI.verifyTextPresent('Jawa timur', false)

WebUI.delay(10)

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Jawa timur_checkmark'))

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Banten_checkmark'))

WebUI.verifyTextPresent('Banten', false)

WebUI.delay(10)

WebUI.check(findTestObject('Produk Sorting Filter/Page_Shop/span_Banten_checkmark'))

WebUI.closeBrowser()

