import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/shop')

WebUI.selectOptionByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Paling Sesuai', false, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyOptionSelectedByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Paling Sesuai', false, 60)

WebUI.delay(5)

WebUI.selectOptionByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Produk A-Z', false, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyOptionSelectedByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Produk A-Z', false, 60)

WebUI.delay(5)

WebUI.selectOptionByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Produk Z-A', false, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyOptionSelectedByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Produk Z-A', false, 60)

WebUI.delay(5)

WebUI.selectOptionByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Termurah', false, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyOptionSelectedByLabel(findTestObject('Produk Sorting Filter/Page_Shop/select_Paling SesuaiProduk A-ZProduk Z-ATermurah'), 
    'Termurah', false, 60)

WebUI.delay(5)

WebUI.closeBrowser()

