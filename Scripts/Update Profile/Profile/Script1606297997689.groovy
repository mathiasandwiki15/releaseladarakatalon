import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths

WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(3)

WebUI.click(findTestObject('UpdateProfileRepository/Page_/div_Mathias'))

WebUI.delay(3)

WebUI.click(findTestObject('UpdateProfileRepository/Page_/a_Pengaturan'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/input__nama_lengkap'), 'Mathias Brait ')

WebUI.delay(3)

WebUI.click(findTestObject('UpdateProfileRepository/Page_Profile/input__nomor_hp'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_tanggal_lahir'), 
    '10', false)

WebUI.verifyOptionPresentByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_tanggal_lahir'), 
    '10', false, 60)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_bulan_lahir'), '1', 
    false)

WebUI.verifyOptionPresentByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_bulan_lahir'), 
    '1', false, 60)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_tahun_lahir'), '1990', 
    false)

WebUI.verifyOptionPresentByValue(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/select_tahun_lahir'), 
    '1990', false, 60)

WebUI.delay(3)

WebUI.check(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/input__gender'))

WebUI.delay(3)

WebUI.click(findTestObject('UpdateProfileRepository/Page_Profile/a_Pilih Gambar'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/UpdateProfileRepository/Page_Profile/input_Selamat_wp-submit'))

WebUI.delay(3)

WebUI.closeBrowser()

