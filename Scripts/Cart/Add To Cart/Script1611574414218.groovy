import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), GlobalVariable.email)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.setText(findTestObject('Wishlist dan Searching Box/Page_/input_tutup_keyword'), 'celana')

WebUI.click(findTestObject('Wishlist dan Searching Box/Page_/span_tutup_glyphicon glyphicon-search sp_searchNow submit-search'))

WebUI.click(findTestObject('Object Repository/Cart/Page_Product/h4_Celana pendek anak Rcr -80 baby frozen'))

WebUI.click(findTestObject('Object Repository/Cart/Page_Jual Celana pendek anak Rcr -80 baby frozen/varian_hitam'))

WebUI.click(findTestObject('Object Repository/Cart/Page_Jual Celana pendek anak Rcr -80 baby frozen/ukuran_30'))

WebUI.click(findTestObject('Object Repository/Cart/Page_Jual Celana pendek anak Rcr -80 baby frozen/span_Sukses menambahkan produk ini ke keranjang belanja Anda._addtocart_detail'))

WebUI.delay(3)

