import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Checkout/Page_/Page_/img'))

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_/a_Lihat keranjang'))

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_Cart/input_Rp 159,000_sub_cartCheckout'))

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_Checkout/a_Pilih Alamat Lain'))

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_Checkout/a_Pilih'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/Pengiriman/Page_Checkout/select_Regular (1 - 3 Hari)'), 1, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyOptionSelectedByIndex(findTestObject('Object Repository/Pengiriman/Page_Checkout/select_Regular (1 - 3 Hari)'), 
    1, 0, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_Checkout/input_Rp 159,000_sub_cartCheckout'))

WebUI.check(findTestObject('Object Repository/Page_Payment/span_Pembayaran Instan_ckm'))

WebUI.click(findTestObject('Object Repository/Payment/Page_Payment/div_Pilih E-Wallet'))

WebUI.click(findTestObject('Page_Payment/div_Ovo'))

WebUI.check(findTestObject('Page_Payment/checkmart_syaratketentuan'))

WebUI.click(findTestObject('Page_Payment/button_bayar_sekarang'))

WebUI.setText(findTestObject('Page_Payment/input_notelp_ovo'), '08123278581')

WebUI.click(findTestObject('Page_Payment/button_submit_notlp_ovo'))

