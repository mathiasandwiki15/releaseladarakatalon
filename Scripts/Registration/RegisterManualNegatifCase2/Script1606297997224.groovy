import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser("https://staging.ladara.id/register#!")
WebUI.setText(findTestObject('Object Repository/RegisterObjectRepository/Page_Register/input_Gagal_u_email'), "mathiasandwiki15@gmail.com")
WebUI.setText(findTestObject('Object Repository/RegisterObjectRepository/Page_Register/input_Masukkan Email_u_password'), "Testing123")
WebUI.setText(findTestObject('Object Repository/RegisterObjectRepository/Page_Register/input_Masukkan Kata Sandi_re_password'), "Testing")
WebUI.setText(findTestObject('Object Repository/RegisterObjectRepository/Page_Register/input_Konfirmasi Kata Sandi tidak sama_u_nama'), "MathiasAndwiki")
WebUI.click(findTestObject('Object Repository/RegisterObjectRepository/Page_Register/input_Silahkan isi nama Anda_user-submit'))