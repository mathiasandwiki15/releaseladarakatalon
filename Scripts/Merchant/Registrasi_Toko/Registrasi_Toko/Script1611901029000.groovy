import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Registrasi_Merchant/Page_/a_Toko'))

WebUI.delay(3)

WebUI.click(findTestObject('Registrasi_Merchant/Page_/button_DaftarkanToko'))

WebUI.delay(3)

WebUI.setText(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_nama_toko'), 'mathiastokotesting')

linktoko = WebUI.getText(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_nama_toko'))

WebUI.verifyMatch(linktoko, linktoko, true)

WebUI.check(findTestObject('Object Repository/Registrasi_Merchant/Page_Register Merchant/input_radio_umum'))

WebUI.verifyElementChecked(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_radio_umum'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_button_Selanjutnya'))

WebUI.delay(5)

WebUI.setText(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_alamat_toko'), 'Komplek Depkes 3. Kalimalang , Bekasi')

WebUI.delay(3)

WebUI.click(findTestObject('Registrasi_Merchant/Page_Register Merchant/b'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_kecamatan'), 'Pondok Gede')

WebUI.sendKeys(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_kecamatan'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Registrasi_Merchant/Page_Register Merchant/input_lokasipickup'), 'Depkes 3')

WebUI.sendKeys(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_lokasipickup'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.check(findTestObject('Registrasi_Merchant/Page_Register Merchant/span_Kebijakan Privasi_checkmark'))

WebUI.click(findTestObject('Registrasi_Merchant/Page_Register Merchant/input_button_bukatokosekarang'))

WebUI.delay(10)

WebUI.closeBrowser()



