import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Random
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.util.Date;

SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");  
Date date = new Date(); 
Calendar c = Calendar.getInstance()
c.setTime(date)
c.add(Calendar.DATE, 1)
date = c.getTime()

Calendar d = Calendar.getInstance()
d.setTime(date)
d.add(Calendar.DATE,30)
datee = d.getTime()


int max = 100 
int min = 50

int random_int = (int)(Math.random() * (max - min + 1) + min)
WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/a_Masuk'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/div_Iklan  Promosi'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/a_buatVoucher'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_namaVoucher'), 'Voucher Baru')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_kodeVoucher'), 'VA'+random_int)

WebUI.check(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/Dalam Rupiah'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/iMaksimum_Potongan_Harga'), '10000')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/Minimum_Pembelian'), '100000')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_kuota'), '2')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_tanggal_mulai'), date_format.format(date))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_tanggal_selesai'), date_format.format(datee))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_waktu_mulai'), '00:00')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_waktu_selesai'), '00:00')

WebUI.check(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/b_Buat Voucher'))

