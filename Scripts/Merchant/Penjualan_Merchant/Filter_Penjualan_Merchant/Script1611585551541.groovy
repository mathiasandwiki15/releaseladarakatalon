import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/a_Masuk'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Selamat datang di Ladara_log'), 'awawaw@ymail.com')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Email_pwd'), 'Ladara100%!')

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.clearText(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/input_Filter_date'))

WebUI.setText(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/input_Filter_date'), "24-Des-2020~13-Jan-2021")

WebUI.delay(5)
