import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Produk Saya'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/div_Daftar Produk'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Atur'))

WebUI.mouseOver(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Edit'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Edit'))

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_panjang'), "10")

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_lebar'), "10")

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_tinggi'), "10")

def panjang = WebUI.getText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_panjang'))

WebUI.verifyMatch(panjang, panjang, true)

def lebar = WebUI.getText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_lebar'))

WebUI.verifyMatch(lebar, lebar, true)

def tinggi = WebUI.getText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_tinggi'))

WebUI.verifyMatch(tinggi, tinggi, true)

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Update'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_OK'))

WebUI.closeBrowser()

