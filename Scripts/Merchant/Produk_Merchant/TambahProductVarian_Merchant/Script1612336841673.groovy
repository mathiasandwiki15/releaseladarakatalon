import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Produk Saya'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Tambah Produk'))

WebUI.delay(5)

CustomKeywords.'newpackage.newkeywoard.uploadFile'(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/span_Gambar Utama'), 
    'C:\\Users\\lenovo\\git\\ladarakatalonweb\\gambarutama.jpg')

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_title'), 'Produk Varian 1')

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_form-control'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_UMKM'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Baju Anak'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/label_Brand                            wajib'))

WebUI.selectOptionByValue(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/select_AJP LABELAtagoAXIS-YBodyshopBrand Ba_272798'), 
    '5556', true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/textarea_wajib_description'), 
    'Produk Memiliki 2 Varian Warna ')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_price'), '100000')

WebUI.delay(3)

// Tambah Varian 1 jenis // 
WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_Tambah Variant'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/nama_variant'), "Warna")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/pilihan_variant'), "Merah")

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/img'))

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/pilihan_variant2'), "Biru") 

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_harga'), "35000")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_stok'), "100")

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_sku'), "SKU12")

WebUI.delay(10)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_hargavar1'), "35000")

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_stok (1)'), "90")

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_sku2'), "SKU13")

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_weight'), '250')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_panjang'), "5")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_lebar'), "5")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_tinggi'), "5")

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.delay(3)

WebUI.closeBrowser()






