import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('PengirimanMerchant/Page_Ladara Merchant/b_Pengaturan Toko (1)'))

WebUI.click(findTestObject('PengirimanMerchant/Page_Ladara Merchant/div_Pengiriman'))

WebUI.delay(2)

WebUI.uncheck(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/Gojek_checkmark'))

WebUI.check(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/Gojek_checkmark'))

WebUI.delay(2)

WebUI.check(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/Grab_checkmark'))

WebUI.delay(2)

WebUI.check(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/TIKI_checkmark'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/button_Simpan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/PengirimanMerchant/Page_Ladara Merchant/button_OK'))

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

