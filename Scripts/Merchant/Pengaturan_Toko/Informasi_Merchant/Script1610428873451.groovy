import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('PengirimanMerchant/Page_Ladara Merchant/b_Pengaturan Toko (1)'))

WebUI.setText(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/input_Slogan_form-control'), 'Maju Bersama Ladara')

WebUI.setText(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/textarea_Deskripsi_form-control'), 
    'Menjual berbagai macam kebutuhan Anda')

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/Page_Ladara Merchant/button_OK'))

WebUI.click(findTestObject('Information_Merchant/Page_Ladara Merchant/button_Buka Toko'))

WebUI.check(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Senin'))

WebUI.verifyElementChecked(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Senin'), 0)

WebUI.delay(3)

WebUI.check(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Selasa'))

WebUI.verifyElementChecked(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Selasa'), 0)

WebUI.delay(3)

WebUI.check(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Rabu'))

WebUI.verifyElementChecked(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_Rabu'), 0)

WebUI.delay(3)

WebUI.check(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_kamis'))

WebUI.verifyElementChecked(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_kamis'), 0)

WebUI.delay(3)

WebUI.check(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_jumat'))

WebUI.verifyElementChecked(findTestObject('Information_Merchant/Page_Ladara Merchant/checkbox_jumat'), 0)

WebUI.delay(3)

WebUI.setText(findTestObject('Information_Merchant/Page_Ladara Merchant/input_jambukatutup'), '09:00 ~ 18:00')

WebUI.click(findTestObject('Information_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.click(findTestObject('Information_Merchant/Page_Ladara Merchant/Page_Ladara Merchant/button_OK'))

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/button_Tutup Toko'))

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/input_Mulai Tanggal_date'))

WebUI.setText(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/input_Mulai Tanggal_date'), '24-12-2021')

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/input_Sampai Tanggal_date'))

WebUI.setText(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/input_Sampai Tanggal_date'), '26-12-2021')

WebUI.setText(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/textarea_Catatan_form-control'), 
    'Libur Natal 2021')

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/Page_Ladara Merchant/button_OK'))

WebUI.click(findTestObject('Information_Merchant/Page_Ladara Merchant/button_Pilih Gambar'))

CustomKeywords.'com.kms.katalon.keyword.uploadfile.UploadFile.uploadFile'(findTestObject('Object Repository/Information_Merchant/Page_Ladara Merchant/button_Pilih Gambar'), 
    'C:\\Users\\lenovo\\git\\ladarakatalonweb\\gambar.jpeg')

WebUI.closeBrowser()

