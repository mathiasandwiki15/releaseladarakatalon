import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Random as Random
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date

////// Pengaturan Toko - Tambah Produk /////

String nama = "yrexumoc-7491@yopmail.com"

WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Produk Saya'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Tambah Produk'))

WebUI.delay(10)

CustomKeywords.'newpackage.newkeywoard.uploadFile'(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/span_Gambar Utama'),
	'C:\\Users\\lenovo\\git\\ladarakatalonweb\\gambarutama.jpg')

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_title'), 'ProdukUMUM1')

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_form-control'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_UMKM'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Baju Anak'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/label_Brand                            wajib'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/section_brand'))

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_brand'), 'Bagus')

WebUI.sendKeys(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_brand'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/textarea_wajib_description'),
	'Deskripsi Produk')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_price'), '100000')

//// Tambah 1 level variant
 
WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_Tambah Variant'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/nama_variant'), "Warna")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/pilihan_variant'), "Merah")

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/img'))

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/tambah_variant/pilihan_variant2'), "Biru")

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_harga'), "100000")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_stok'), "100")

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_sku'), "SKU12")

WebUI.delay(10)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_hargavar1'), "100000")

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_stok (1)'), "90")

WebUI.setText(findTestObject('Object Repository/TambahVariant/Page_Ladara Merchant/input_sku2'), "SKU13")

////

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_weight'), '250')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_panjang'),
	'5')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_lebar'),
	'5')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_tinggi'),
	'5')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_OK'))

WebUI.closeBrowser()

////// Pengaturan Penambahan Voucher /////

SimpleDateFormat date_format = new SimpleDateFormat('dd-MM-yyyy')
time_format = new SimpleDateFormat("HH:mm")

Date date = new Date()

Calendar c = Calendar.getInstance()

c.setTime(date)

c.add(Calendar.DATE,0)

date = c.getTime()

Calendar d = Calendar.getInstance()

d.setTime(date)

d.add(Calendar.DATE, 30)

datee = d.getTime()

Calendar e = Calendar.getInstance()

e.setTime(date)

e.add(Calendar.DATE, 0)

time = e.getTime()


int max = 100

int min = 50

int random_int = (((Math.random() * ((max - min) + 1)) + min) as int)


WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/div_Iklan  Promosi'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/a_buatVoucher'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_namaVoucher'), 'Voucher Baru')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_kodeVoucher'), 'VAR' + random_int)

WebUI.check(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/Dalam Rupiah'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/iMaksimum_Potongan_Harga'), '10000')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/Minimum_Pembelian'), '50000')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_kuota'), '50')

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_tanggal_mulai'), date_format.format(date))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_tanggal_selesai'), date_format.format(datee))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_waktu_mulai'), time_format.format(time))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input_waktu_selesai'), '00:00')

WebUI.check(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/input'))

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/b_Buat Voucher'))

WebUI.delay(5)

WebUI.navigateToUrl("https://release.ladara.id/merchant/#/iklan-promosi/voucher")

WebUI.delay(7)

WebUI.closeBrowser()

/// Penambahan Diskon Product ///
WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Ladara Merchant/div_Iklan  Promosi'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/a_Discount'))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_namaDiscount'), 'Diskon Awal Tahun')

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Tanggal Mulai_date'), date_format.format(
		date))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Tanggal Selesai_date'),
	date_format.format(datee))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Waktu Mulai_date'), time_format.format(time))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Waktu Selesai_date'), '00:00')

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Buat Diskon'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Tambah Produk'))

WebUI.check(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Konfirmasi'))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Harga Diskon wajib diisi_percent'),
	'10')

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Diskon wajib diisi_pembelian_1'),
	'5')

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Selesai'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_OK'))

WebUI.closeBrowser()

///// Pembelian Produk UMKM Oleh User ////
WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://release.ladara.id/product-market/umkm')

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/product_sample_umummerchant'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/pilih_varian_biru'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_Jual DekranasFashionD/button_beliSekarang'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Cart/button_checkout'))

WebUI.delay(5)

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/a_Pilih Alamat Lain'))

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/a_Pilih'))

WebUI.selectOptionByIndex(findTestObject('Checkout/Checkout_Before_Payment/Page_Checkout/select_shipping_method'),
	1, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/button_input_voucher'))

WebUI.delay(3)

WebUI.setText(findTestObject('Checkout/Checkout_Before_Payment/kode_Voucher'), "VAR"+random_int)

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Checkout/Checkout_Before_Payment/kode_Voucher'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/Page_Checkout/button_terapkan'))

WebUI.delay(3)

WebUI.click(findTestObject('Checkout/Checkout_Before_Payment/Page_Checkout/input_button_pilihpembayaran'))

WebUI.check(findTestObject('Object Repository/Checkout/Page_Payment/span_Kartu Kredit_ckm'))

WebUI.check(findTestObject('Object Repository/Checkout/Page_Payment/span_Syarat  Ketentuan_checkmark'))

WebUI.click(findTestObject('Object Repository/Checkout/Page_Payment/input_Syarat  Ketentuan_sub_cartCheckout mob_sub_paymentNow'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout invoice-978/button_Klik disini untuk simulasi pembayara_9bca1a'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout invoice-978/button_Bayar Sekarang'))

WebUI.delay(20)

// Menerima Pesanan Oleh User

WebUI.openBrowser('https://release.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/a_Masuk'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Pesanan Baru'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/button_terimapesanan'))

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/button_yakin'))

WebUI.delay(5)

WebUI.closeBrowser()

// Melihat Status Pembelian

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://release.ladara.id/my-order')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_Diproses'))

WebUI.verifyTextPresent("Diproses", false)

WebUI.delay(5)

// Penjual Request PickUp

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_siapdikirim'))

WebUI.click(findTestObject('Object Repository/Status Pembelian/button_Request_pickup'))

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/button_yakin'))

WebUI.delay(5)

WebUI.closeBrowser()


// Pembeli Konfirmasi Pembelian

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), "mathias.brait@idstar.co.id")

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://release.ladara.id/my-order')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_dikirim'))

WebUI.delay(3)

WebUI.closeBrowser()

// Penjual Download Report Penjualan

WebUI.openBrowser('https://release.ladara.id/')

WebUI.click(findTestObject('LoginObjectRepository/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Status Pembelian/button_download_report_penjualan'))

